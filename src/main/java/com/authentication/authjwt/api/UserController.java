package com.authentication.authjwt.api;

import com.authentication.authjwt.dto.User;

public interface UserController {
    User login(String username, String pwd);
}
