package com.authentication.authjwt.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldControllerImp implements HelloWorldController {

    @Override
    @RequestMapping("hello")
    public String helloWorld() {
        return "Hello world!!";
    }
    
}
